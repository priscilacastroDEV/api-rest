/FUENTE_SERVIDOR:
-----	Almacenando los datos en una Base de Datos	--------

*Se creo la carpeta: Service y dentro la clase llamda "UsuarioService.java"
*Se creo la carpeta: repository y dentro la clase llamda "UsuarioRepository.java"
*Se hizo uso de las clases: DatosUsuario.java y PrincipalWebService.java

*Si se desea cambiar a un servidor en especifico para el almacenamiento de los datos, modificar los siguientes parametros en el archivo "application.properties" con sus respectivos valores:

sspring.datasource.url=jdbc:mysql://35.223.247.249:3306/saveUser #Cambiar el srvidor(35.223.247.249):(puerto)/(BaseDeDatos)
spring.datasource.username=root #Tu usuario
spring.datasource.password=tu_pass #Tu contraseña

NOTA:
Como no cuento con un servidor local he creado una instancia temporal MySQL 8 en GCP, así que es importante cambiar esta parte de la configuración y tomar en cuenta que el código esta listo para usar MySQL 8.
Una vez modificado esto, genrar el jar nuevamente para poder correrlo y ver los datos almacenados en su base de datos.

De igual manera si se desea solo ejecutar el jar tal cual esta en ejecutable, en la consola se mostrar la query usada para el insert.


//Ejecutable
*En esta carpeta encontraras el jar listo para correr (tomando en cuenta lo anterior), cambiando su version con el nombre de: "challengeajax-0.0.2-SNAPSHOT.jar".

*Ejecutar el jar: java -jar challengeajax-0.0.2-SNAPSHOT.jar


//test_paises
*Aquí se encuentra la aplicacion lista para ejecutarse junto al código, solo abriendo el index.html