package mx.sisu.challengeajax;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengeAjaxApplication.class)
public class ChallengeAjaxApplicationTests {
	@Test
	public void contextLoads() {
	}
}
