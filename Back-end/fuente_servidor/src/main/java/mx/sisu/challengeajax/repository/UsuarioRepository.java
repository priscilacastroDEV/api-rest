package mx.sisu.challengeajax.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.sisu.challengeajax.dto.DatosUsuario;

//Repository para almacenar en la BD con JpaRepository
public interface UsuarioRepository extends JpaRepository<DatosUsuario, Integer> {
}
