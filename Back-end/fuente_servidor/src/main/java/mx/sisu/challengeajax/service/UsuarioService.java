package mx.sisu.challengeajax.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sisu.challengeajax.dto.DatosUsuario;
import mx.sisu.challengeajax.repository.UsuarioRepository;

//Clase Service para almacenar en la BD
@Service
public class UsuarioService {
    @Autowired
    private final UsuarioRepository usuarioRepository;

    // Implementando el Repository
    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    // Craar el metodo para guardar los datos almacenados en la clase DatosUsuario
    public DatosUsuario guardarDatosUsuario(DatosUsuario datosUsuario) {
        return usuarioRepository.save(datosUsuario);
    }
}
