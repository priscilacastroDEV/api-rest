//Script para consumir API
document.addEventListener("DOMContentLoaded", function () {
  // URL de la API para obtener la lista de países
  const apiUrlPaises = "http://localhost:8080/servicio/paises";

  // Obtener la lista de países
  fetch(apiUrlPaises)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Error al obtener la lista de países");
      }
      return response.json();
    })
    .then((data) => {
      // Llenar el dropdown de países
      const dropdownPaises = document.getElementById("pais");
      data.forEach((pais) => {
        const option = document.createElement("option");
        option.text = pais.nombre;
        option.value = pais.id;
        dropdownPaises.appendChild(option);
      });
      // Obtener ID del país seleccionado
      dropdownPaises.addEventListener("change", function () {
        const paisIdSeleccionado = parseInt(this.value, 10);
        cargarEstados(paisIdSeleccionado);
      });
    })
    .catch((error) => {
      console.error("Error al obtener la lista de países:", error);
    });
});

//---------------------------- Cargar los estados según el país seleccionado ----------------
function cargarEstados(paisId) {
  // URL de la API para obtener los estados
  const apiUrlEstados = "http://localhost:8080/servicio/estados";

  // Limpiar las opciones anteriores del dropdown de estados
  const dropdownEstados = document.getElementById("estado");
  dropdownEstados.innerHTML = "";

  // Agregar la opción "Selecciona un estado" como valor por default
  const optionSeleccionar = document.createElement("option");
  optionSeleccionar.text = "Selecciona un estado";
  optionSeleccionar.value = "";
  dropdownEstados.appendChild(optionSeleccionar);

  // Limpiar el dropdown de ciudades una vez seleccionado un país o estado diferente
  const dropdownCiudades = document.getElementById("ciudad");
  dropdownCiudades.innerHTML = ""; // Limpiar opciones anteriores
  const optionSeleccionarCd = document.createElement("option");
  optionSeleccionarCd.text = "Selecciona una ciudad";
  optionSeleccionarCd.value = "";
  dropdownCiudades.appendChild(optionSeleccionarCd);

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(paisId),
  };

  // Realizar la solicitud POST para obtener los estados
  fetch(apiUrlEstados, requestOptions)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Error al obtener la lista de estados");
      }
      return response.json();
    })
    .then((data) => {
      data.forEach((estado) => {
        const option = document.createElement("option");
        option.text = estado.nombre;
        option.value = estado.id;
        dropdownEstados.appendChild(option);
      });
      // Obtener ID del Estado seleccionado
      dropdownEstados.addEventListener("change", function () {
        const estadoIdSeleccionado = parseInt(this.value, 10);
        cargarCiudades(estadoIdSeleccionado);
      });
    })
    .catch((error) => {
      console.error("Error al obtener la lista de estados:", error);
    });
}

//----------------------------------Cargar las ciudades según el estado seleccionado ---------------------
function cargarCiudades(estadoId) {
  // URL de la API para obtener los estados
  const apiUrlCiudades = "http://localhost:8080/servicio/ciudades";

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(estadoId),
  };

  // Realizar la solicitud POST para obtener las ciudades
  fetch(apiUrlCiudades, requestOptions)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Error al obtener la lista de ciudades");
      }
      return response.json();
    })
    .then((data) => {
      // Llenar el dropdown de ciudades
      const dropdownCiudades = document.getElementById("ciudad");
      // Limpiar opciones anteriores
      dropdownCiudades.innerHTML = "";
      const optionSeleccionar = document.createElement("option");
      optionSeleccionar.text = "Selecciona una ciudad";
      optionSeleccionar.value = "";
      dropdownCiudades.appendChild(optionSeleccionar);

      data.forEach((ciudad) => {
        const option = document.createElement("option");
        option.text = ciudad.nombre;
        option.value = ciudad.id;
        dropdownCiudades.appendChild(option);
      });
      // Obtener ID de la ciudad seleccionada
      dropdownCiudades.addEventListener("change", function () {
        const ciudadIdSeleccionado = parseInt(this.value, 10);
      });
    })
    .catch((error) => {
      console.error("Error al obtener la lista de ciudades:", error);
    });
}

//---------------------------- Guardar nuevo usurio ----------------------------
function enviarDatos() {
  // Obtener los valores del formulario
  var ciudad = parseInt(document.getElementById("ciudad").value);
  var nombre = document.getElementById("nombre").value.trim();
  var edad = parseInt(document.getElementById("edad").value);
  var errorNombre = document.getElementById("errorNombre");
  var errorEdad = document.getElementById("errorEdad");

  // Validar nombre
  const regex = /^[A-Za-záéíóúÁÉÍÓÚ\s.]+$/; //Expresion regular

  if (nombre.length > 50 || !regex.test(nombre)) {
    errorNombre.style.display = "block";
    return;
  } else {
    errorNombre.style.display = "none";
  }
  // Validar edad
  if (edad < 18 || edad > 99) {
    errorEdad.style.display = "inline";
    return;
  } else {
    errorEdad.style.display = "none";
  }

  // Crear un objeto con los datos a enviar
  var datos = {
    ciudadId: ciudad,
    nombre: nombre,
    edad: edad,
  };

  console.log(datos);
  //Peticion POST
  const url = "http://localhost:8080/servicio/guardar"; //url POST
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(datos),
  })
    .then(function (response) {
      if (!response.ok) {
        throw new Error("HTTP error " + response.status);
      }
      return response.json();
    })
    .then(function (data) {
      console.log(data);
      // Eliminar el formulario existente
      var form = document.getElementById("form");
      form.parentNode.removeChild(form);

      // Crear un nuevo elemento <h1> con el mensaje de respuesta
      var h1 = document.createElement("h1");
      h1.textContent = data.resultado;

      // Agregar el nuevo elemento al cuerpo del documento
      document.body.appendChild(h1);
    })
    .catch(function (error) {
      console.error("Error:", error);
    });
}
